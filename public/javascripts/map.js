var map = L.map('main_map').setView([4.710856, -74.049281], 13);
var dilanIcon = L.icon({
    iconUrl: 'assets/img/dilan.png',
    iconSize: [40, 40],
    iconAnchor: [20, 20],
    popupAnchor:  [0, -10]
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([4.7108, -74.0492], {icon: dilanIcon}).addTo(map)
    .bindPopup('<h5 class="text-center">Mi casita</h5><b>Estado:</b> Con Lulo')

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
                .bindPopup('<h5 class="text-center"><b>'+bici.id+'</b>: '+bici.modelo+'</h5><b>Color:</b> '+bici.color)
        });
    }
})