var mongooseAPI = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', () => {

    beforeAll(function(done){
        mongooseAPI.disconnect(); 
        done();
    });

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongooseAPI.connect(mongoDB, {useNewUrlParser: true});

        const db = mongooseAPI.connection;
        db.on('error',console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('We are connected to the test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err);
            mongooseAPI.disconnect(err); 
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 201', (done) => {
            var headers = {'Content-type': 'application/json'};
            var aBici = '{"code":"10", "color":"Gris", "modelo":"Urbana", "lat":4.71, "lng":-74.06}';

            request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/create',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(201);
                    Bicicleta.findByCode(10,function(err,tBici){
                        expect(tBici.code).toBe(10);
                        expect(tBici.color).toBe("Gris");
                        done();
                    });
                });
        });
    });

    describe('PUT BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var aBici = new Bicicleta({code: 10, color:"Verde", modelo:"Urbana", ubicacion:[5,-74]});
            Bicicleta.add(aBici, function(err,newBicis){
                if(err) console.log(err);
                var headers = {'Content-type': 'application/json'};
                var jsonBici = '{"code":"10", "color":"Azul", "modelo":"Urbana", "lat":4.71, "lng":-74.06}';

                request.put({
                        headers: headers,
                        url: 'http://localhost:3000/api/bicicletas/update',
                        body: jsonBici
                    }, function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        Bicicleta.findByCode(10,function(err,tBici){
                            expect(tBici.code).toBe(10);
                            expect(tBici.color).toBe("Azul");
                            done();
                        });
                });
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var aBici = new Bicicleta({code: 10, color:"Verde", modelo:"Urbana", ubicacion:[5,-74]});
            Bicicleta.add(aBici, function(err,newBicis){
                if(err) console.log(err);

                var headers = {'Content-type': 'application/json'};
                var jsonBici = '{"code":"10"}';

                request.delete({
                        headers: headers,
                        url: 'http://localhost:3000/api/bicicletas/delete',
                        body: jsonBici
                    }, function(error, response, body){
                        expect(response.statusCode).toBe(204);
                        Bicicleta.allBicis(function(err,bicis){
                            expect(bicis.length).toBe(0);
                            done();
                        });
                });
            });
        });
    });
});