var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');

describe('Testing Bicicletas - ', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('We are connected to the test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance: ', () => {
        it('Crear instancia de bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, 'Rojo', 'Urbana', [4.7, -74.05]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Rojo');
            expect(bici.modelo).toBe('Urbana');
            expect(bici.ubicacion[0]).toBe(4.7);
            expect(bici.ubicacion[1]).toBe(-74.05);
            done();
        });
    });

    describe('Bicicleta.allBicis: ', () => {
        it('Coleccion vacia al inicio', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add: ', () => {
        it('Agregar solo una Bici', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici = new Bicicleta({code: 0, color:"Verde", modelo:"Urbana",ubicacion:[4,-74]});
                Bicicleta.add(aBici, function(err,newBicis){
                    if(err) console.log(err);
                    Bicicleta.allBicis(function(err,bicis){
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(aBici.code);
                        expect(bicis[0].color).toBe(aBici.color);
                        done();
                    });
                });
            });
        });
    });

    describe('Bicicleta.findById: ', () => {
        it('Bici con code = 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici0 = new Bicicleta({code: 0, color:"Verde", modelo:"Urbana"});
                Bicicleta.add(aBici0, function(err,newBicis){
                    if(err) console.log(err);

                    var aBici1 = new Bicicleta({code: 1, color:"Azul", modelo:"Urbana"});
                    Bicicleta.add(aBici1, function(err,newBicis){
                        if(err) console.log(err);

                        var aBici2 = new Bicicleta({code: 2, color:"Azul", modelo:"Urbana"});
                        Bicicleta.add(aBici2, function(err,newBicis){
                            if(err) console.log(err);
                            Bicicleta.findByCode(1,function(err,tBici){
                                expect(tBici.code).toBe(aBici1.code);
                                expect(tBici.color).toBe(aBici1.color);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});

/*beforeEach(()=>{Bicicleta.allBicis = [];})

describe("Bicicleta.allBicis", () => {
    it("Comienza vacia", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
}); 

describe("Bicicleta.add", () => {
    it("Agregar 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'Rojo', 'Urbana', [4.7, -74.05]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a)
    });
}); 
   
describe("Bicicleta.findById", () => {
    it("Agregar 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(0, 'Rojo', 'Urbana', [4.7, -74.05]);
        var b = new Bicicleta(1, 'Verde', 'Montaña', [4.72, -74.07]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var target = Bicicleta.findById(1)
        expect(target.id).toBe(1);
        expect(target).toBe(b)
    });
}); 

describe("Bicicleta.removeById", () => {
    it("Agregar 1", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(0, 'Rojo', 'Urbana', [4.7, -74.05]);
        var b = new Bicicleta(1, 'Verde', 'Montaña', [4.72, -74.07]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        expect(Bicicleta.allBicis.length).toBe(2);
        Bicicleta.removeById(0)
        
        expect(Bicicleta.allBicis.length).toBe(1);

        var target = Bicicleta.findById(1)
        expect(target.id).toBe(1);
        expect(target).toBe(b);

        var errMess = ''

        try {
            var target = Bicicleta.findById(0)            
        } catch (error) {
            errMess = error.message;
        }    
        expect(errMess).toBe('No existe una bici con el Id 0')
    });
}); */